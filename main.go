/*
Copyright © 2022 Emanuel

*/
package main

import "myapp/cmd"

func main() {
	cmd.Execute()
}
