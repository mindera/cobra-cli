APP = myApp
clean:
	rm $(APP)
build:
	go build -o $(APP) main.go 

run:
	./$(APP)	

runHelp:
	./$(APP) --help

runSumHelp: 
	./$(APP) sum -h

runSum: 
	./$(APP) sum 3 3

runSumWithOneArg: 
	./$(APP) sum 3

runSumWithOneArgAsFloat: 
	./$(APP) sum 3 3.0

runGetJokes: 
	./$(APP) jokes -h

runGetJokesWithASuggestionHardcoded: 
	./$(APP) cenas

runGetJokesWithASuggestionByCobra: 
	./$(APP) joke

runKanyeTweetHelp: 
	./$(APP) jokes kanye -h

runKanyeTweet: 
	./$(APP) jokes kanye

runKanyeTweetWithFlagFromParentToUppercase: 
	./$(APP) jokes -u kanye
	
runRealJoke: 
	./$(APP) jokes realJoke

runRealJokePun: 
	./$(APP) jokes realJoke -j Pun

runRealJokeProgramming: 
	./$(APP) jokes realJoke -j Programming

runRealJokeProgrammingToUppercase: 
	./$(APP) jokes -u realJoke -j Programming
