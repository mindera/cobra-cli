/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"strings"

	"github.com/spf13/cobra"
)

// getJokeCmd represents the getJoke command
var GetRealJokeCmd = &cobra.Command{
	Use:   "realJoke <amount of jokes>",
	Short: "Gets a joke",
	Long: `
	Gets a joke long explanation
	retrieved by an api : "https://v2.jokeapi.dev/joke/"
	You can any of these flags [Programming, Misc, Dark, Pun, Spooky, Christmas] with -j or --jokeType	
	`,
	RunE: func(cmd *cobra.Command, args []string) error {
		toggleValue, err := cmd.Flags().GetBool("upperCase")
		if err != nil {
			return err
		}
		jokeType, err := cmd.Flags().GetString("jokeType")
		if err != nil {
			cmd.Println("Bad joke type given")
			return err
		}
		cmd.Println("JokeType: ", jokeType)
		joke, err := getRealJoke(jokeType)
		if err != nil {
			return err
		}
		if toggleValue {
			joke = strings.ToUpper(joke)
		}
		cmd.Println(joke)

		return nil
	},
	Example: "myapp getJokes getRealJoke",
	Args:    cobra.ExactArgs(0),
}

func init() {
	// flag to get a specific type of joke[Programming, Misc, Dark, Pun, Spooky, Christmas]
	GetRealJokeCmd.Flags().StringP("jokeType", "j", "Any", "The type of joke you want")

}
