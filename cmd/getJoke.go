/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"github.com/spf13/cobra"
)

// GetJokes represents the getTweet command
var GetJokes = &cobra.Command{
	Use:   "jokes",
	Short: "Get a joke from child commands",
	Long: `
		This should be a longer description of usage of the getJoke command
	`,

	Run: func(cmd *cobra.Command, args []string) {
		cmd.Printf("Args: %#v\n", args)
		toggleValue, err := cmd.Flags().GetString("")

		if err != nil {
			cmd.Println("local toggle flag not received")
		}
		cmd.Println("local toggle flag received with parameter: ", toggleValue)
	},
	// Enforce a suggestion
	SuggestFor: []string{"cenas"},
	Args:       cobra.ExactArgs(1),
	Example:    "myapp getJokes",
}
