package cmd

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const (
	JokeAPI  = "https://v2.jokeapi.dev/joke/"
	KanyeAPI = "https://api.kanye.rest"
)

type Quote struct {
	Quote string `json:"quote"`
}

type ToFetch interface {
	*Joke | *Quote
}

type Joke struct {
	Error    bool   `json:"error"`
	Type     string `json:"type"`
	Setup    string `json:"setup"`
	Delivery string `json:"delivery"`
	Joke     string `json:"joke"`
}

func (j *Joke) String() string {

	if j.Type == "twopart" {
		return fmt.Sprintf("%s\n%s", j.Setup, j.Delivery)
	}
	return j.Joke
}

func getKanyeQuote() (string, error) {
	var q Quote
	err := makeRequest(KanyeAPI, &q)

	if err != nil {
		return "", err
	}

	return q.Quote, nil
}

func getRealJoke(jokeType string) (string, error) {
	var joke Joke
	url := JokeAPI + jokeType

	err := makeRequest(url, &joke)
	if err != nil {
		return "", err
	}

	return joke.String(), nil
}

func makeRequest[T ToFetch](url string, f T) error {
	resp, err := http.Get(url)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	err = json.Unmarshal(body, f)
	if err != nil {
		return err
	}
	return nil
}
