/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"os"

	"github.com/spf13/cobra"
)

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "myApp",
	Short: "Top level command",
	Long:  ` An example of a simple top level command long explanation`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	rootCmd.AddCommand(sumCmd, GetJokes)
	// Add sub commands
	GetJokes.AddCommand(kanyeTweetCmd, GetRealJokeCmd)
	// Only available on this command
	GetJokes.Flags().StringP("toggle", "t", "empty", "Help message for toggle locally")
	// Available on this command and its subcommands
	GetJokes.PersistentFlags().BoolP("upperCase", "u", false, "Uppercase the sub-commands")
}
