/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"strconv"

	"github.com/spf13/cobra"
)

const (
	Base    = 10
	BitSize = 64
)

// sumCmd represents the sum command
var sumCmd = &cobra.Command{
	// Display the usage of the command
	Use:   "sum [int] [int]",
	Short: "Sums to give elements",
	// Display the description when the flag --help/-h is used
	Long: `Given two Integers space separated , will return it's sum.
	`,
	RunE: func(cmd *cobra.Command, args []string) error {
		x, err := strconv.ParseInt(args[0], Base, BitSize)
		if err != nil {
			return err
		}
		y, err := strconv.ParseInt(args[1], Base, BitSize)
		if err != nil {
			return err
		}
		cmd.Println(x + y)
		return nil
	},

	// Display the Examples
	Example: "myapp sum 1 2",
	// restricts the number of arguments that the command accepts
	Args: cobra.ExactArgs(2),
	// Could be needed if the command accepts a range of arguments like filepaths
	// Args: cobra.RangeArgs(0, 10),
}
