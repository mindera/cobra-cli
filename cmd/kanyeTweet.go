/*
Copyright © 2022 NAME HERE <EMAIL ADDRESS>

*/
package cmd

import (
	"strings"

	"github.com/spf13/cobra"
)

// kanyeTweetCmd represents the kanyeTweet command
var kanyeTweetCmd = &cobra.Command{
	Use:   "kanye",
	Short: "Get a Kanye West tweet",
	Long: `
		Just some wisdom from Kanye West, retrieved via an API:
			"https://api.kanye.rest"
	`,
	RunE: func(cmd *cobra.Command, args []string) error {
		toggleValue, err := cmd.Flags().GetBool("upperCase")
		if err != nil {
			return err
		}
		quote, err := getKanyeQuote()
		if err != nil {
			return err
		}
		if toggleValue {
			quote = strings.ToUpper(quote)
		}

		cmd.Println(quote)
		return nil
	},
	Args: cobra.ExactArgs(0),
}
