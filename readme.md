# Cobra CLI

projects using cobra
<https://github.com/spf13/cobra/blob/master/projects_using_cobra.md>

```sh
go install github.com/spf13/cobra-cli@latest
cobra-cli init "project name"
cd "project name"
go mod init "project name"
```
